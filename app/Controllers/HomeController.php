<?php

namespace ThemeXpert\Controllers;

use ThemeXpert\Faker\Util;
use Intervention\Image\ImageManager;
use Sun\Contracts\Filesystem\Filesystem;
use ThemeXpert\Faker\Contracts\Image as Faker;
use ThemeXpert\Faker\Contracts\XML as FakerXML;

class HomeController extends Controller
{
    /**
     * Faker image instance.
     *
     * @var \ThemeXpert\Faker\Contracts\Image
     */
    protected $faker;

    /**
     * Faker XML instance.
     *
     * @var \ThemeXpert\Faker\Contracts\XML
     */
    protected $fakerXML;

    /**
     * Instance of util class.
     *
     * @var \ThemeXpert\Faker\Util
     */
    protected $util;

    /**
     * Instance of intervention.
     *
     * @var \Intervention\Image\ImageManager
     */
    protected $intervention;

    /**
     * Filesystem instance.
     *
     * @var \Sun\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * @param \ThemeXpert\Faker\Contracts\Image $faker
     * @param \ThemeXpert\Faker\Contracts\XML   $fakerXML
     * @param \ThemeXpert\Faker\Util            $util
     * @param ImageManager                      $intervention
     * @param Filesystem                        $filesystem
     */
    public function __construct(Faker $faker, FakerXML $fakerXML, Util $util, ImageManager $intervention, Filesystem $filesystem)
    {
        $this->faker = $faker;

        $this->fakerXML = $fakerXML;

        $this->util = $util;

        $this->intervention = $intervention;

        $this->filesystem = $filesystem;
    }

    /**
     * To show home page.
     *
     * @param null $size
     * @param null $background
     * @param null $textColor
     *
     * @return mixed
     */
    public function getIndex($size = null, $background = null, $textColor = null)
    {
        $this->util->sendGoogleAnalytics();

        if(!empty(request()->input('src'))) {
            return $this->faker->responseWithFakeImage(null, null, null, null, request()->input('src'));
        } elseif(!empty(request()->input('text')) or !is_null($size)) {
            return $this->faker->responseWithFakeImage($size, $background, $textColor, request()->input('text'));
        } else {
            return view('home');
        }
    }

    /**
     * Process given XML file.
     *
     * @return mixed
     */
    public function postIndex()
    {
        # only accpet xml file
        $name = request()->file('xml')['name'];
        $extension = end((explode(".", $name)));

        if($extension !== 'xml') {
          return redirect()->back();
        }

        $xml = file_get_contents(request()->file('xml')['tmp_name']);
        $fileName = request()->file('xml')['name'];

        $this->fakerXML->downloadFakeXML($xml, $fileName);
    }

    /**
     * Show image uploader page.
     */
    public function showImageUploader()
    {
        return view('imageUploader');
    }


    /**
     * Upload image.
     */
    public function uploadImage()
    {
        $directoryID = $this->faker->generateFakeImageZip($_FILES);

        return response()->json([
            'statusCode' => 200,
            'message' => 'Uploaded',
            'image_history_id' => $directoryID]);
    }


    /**
     * Download fake image zip.
     */
    public function downloadFakeImageZip($id)
    {
        if($this->faker->fakeImageZipFileExists($id)) {
            return response()->download(
              storage_path(). "/app/uploads/{$id}.zip"
            );
        }
    }


    /**
     * Create image upload directory
     */
    public function createImageUploadDirectory()
    {
        return [
          'directoryName' => $this->faker->getImageUploadDirName()
        ];
    }
}
