import React            from 'react';
import ReactDOM         from 'react-dom';
import ImageUploader    from './components/ImageUploader';

/**
 * Toastr settings
 */
toastr.options.preventDuplicates = true;
toastr.options.closeButton = true;
toastr.options.positionClass = "toast-bottom-right";


/**
 * Mount ImageUploader component
 */
if(document.querySelector('#image-uploader') !== null) {
    ReactDOM.render(<ImageUploader />, document.querySelector('#image-uploader'));
}
