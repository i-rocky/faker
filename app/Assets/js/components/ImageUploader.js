import React        from 'react';
import Event        from './event';
import Request      from 'superagent';
import Dropzone     from 'react-dropzone';
import Clipboard    from 'clipboard';


class ImageUploader extends React.Component {
    /**
     * Create a new image uploader instance.
     */
    constructor() {
        super();

        this.state = {
            loading: false,
            hasDownloadLink: false,
            downloadUrl: null,
            hostName: window.url,
            progressBarStyle: {
                width: "0%"
            }
        }
    }


    /**
     * Add event listener for the image uploader event.
     */
    componentWillMount() {
        Event.on('start_loading', ()  => {
            this.setState({loading: true, hasDownloadLink: false})
        });

        Event.on('progress', (progressBar)  => {
            this.setState({progressBarStyle: {width: progressBar.percent + "%"}})
        });

        Event.on('finished_loading', (data)  => {
            this.setState({loading: false, hasDownloadLink: true, downloadUrl: data.downloadUrl})
            this.getImageUploadDirectory();
        });

        Event.on('error_in_image_type', ()  => {
            this.setState({loading: false, hasDownloadLink: false})
        });

        new Clipboard('.btn-clipboard');

        this.getImageUploadDirectory();
    }


    /**
     * Remove event listener.
     */
    componentDidMount() {
        Event.removeListener('start_loading', () => { });

        Event.removeListener('progress', () => { });

        Event.removeListener('finished_loading', () => { });

        Event.removeListener('error_in_image_type', () => { });
    }


    /**
     * Render jsx.
     *
     * @returns {XML}
     */
    render() {
        let uploaderView = null;
        let downloadView = null;

        if (!this.state.loading) {
            uploaderView = this.renderImageUploader();
        } else {
            uploaderView = this.renderLoadingJsx();
        }

        if (this.state.hasDownloadLink) {
            downloadView = this.renderDownloadLink();
        } else {
            downloadView = this.renderAds();
        }

        return (
            <div>
                <div className="col-md-6 wp-img-dis">
                    <h2 className="text-center title">Upload your images</h2>
                    {uploaderView}
                </div>

                <div className="col-md-6 img-placehold">
                    {downloadView}
                </div>
            </div>
        )
    }


    /**
     * Render image uploader jsx.
     *
     * @returns {XML}
     */
    renderImageUploader() {
        return (
            <div>
                <Dropzone onDrop={this.onDrop} className="image-uploader--dropzone">
                    <h2>
                        Drag & Drop files here
                        <br />
                        <span>(only .JPEG, .JPG & .PNG accepted)</span>
                    </h2>
                </Dropzone>
            </div>
        );
    }


    /**
     * Render loading jsx.
     *
     * @returns {XML}
     */
    renderLoadingJsx() {
        return (
            <div>
                <div className="progress">
                    <div className="progress-bar"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style={this.state.progressBarStyle}>
                    {this.state.progressBarStyle.width}
                    </div>
                </div>
            </div>
        );
    }


    /**
     * Render zip file download link for the fake images.
     *
     * @returns {XML}
     */
    renderDownloadLink() {
        return (
            <div>
                <h2 className="text-center title">
                    Created
                </h2>

                <h3 class="text-center sub-title">
                    It will be deleted from server in 30 minutes form now. So please download and save it in your drive before it expire.
                </h3>

                <div className="download-section">
                    <a className="btn btn-success btn-large" href={this.state.hostName + this.state.downloadUrl}>
                        <span className="glyphicon glyphicon-download-alt"></span>
                        Download Zip
                    </a>
                </div>

                {this.renderClipboardJsx()}
            </div>
        )
    }


    /**
     * Render clipboard jsx.
     *
     * @returns {XML}
     */
    renderClipboardJsx() {
        return (
            <div className="clipboard">
                <span>Copy zip file link (note that download will expire in 30 minutes)</span>
                <div className="input-group">
                    <input type="text"
                        className="form-control"
                        value={this.state.hostName + this.state.downloadUrl}
                        id="download-link"/>

                    <span className="input-group-btn btn-clipboard"
                        data-clipboard-target="#download-link"
                        onClick={this.copied}>

                        <button className="btn btn-default" type="button">
                            <i className="fa fa-clipboard" aria-hidden="true"></i>
                        </button>
                    </span>
                </div>
            </div>
        )
    }


    /**
     * Render Quix page builder ads.
     *
     * @returns {XML}
     */
    renderAds() {
        return (
            <div className="faker-ads">
                <a href="https://www.themexpert.com/?utm_campaign=Joomla&utm_medium=Banner&utm_source=Faker">
                    <img src="/assets/images/ads/quix-fb-h.jpg" />
                 </a>
            </div>
        );
    }


    /**
     * On drop function responsible for
     * uploading image to the server.
     *
     * @param files
     */
    onDrop(files) {

        // broadcast image loading is started
        Event.emit('start_loading');

        let success = false;
        let totalImages = files.length;
        let totalImageProcess = 1;
        let processCompleted = 0;

        // checks image files are valid
        files.forEach((file)=> {
            if ((file.type == 'image/jpeg')
                || (file.type == 'image/jpg')
                || (file.type == 'image/png')) {

                success = true;
            } else {
                toastr.error('Only .JPEG, .JPG & .PNG accepted!!!', 'Error');

                success = false;

                Event.emit('error_in_image_type');
            }
        });

        // if there is no problem when image uploaded,
        // then send a request to the server for generating
        // zip file for the fake images
        files.forEach( (file) => {
            if (success) {
                let req = Request.post('/uploader');
                req.set( { directory: window.directory } );

                req.attach(file.name, file);

                req.end((err, res) => {
                    if (err) toastr.error(err, 'Error');

                    if (res.ok) {
                        let data = JSON.parse(res.text);

                        if (data.statusCode === 200) {

                            processCompleted++;

                            // broadcast image upload finished event with zip file download url
                            if (totalImageProcess === totalImages) {
                                Event.emit('finished_loading', {
                                    downloadUrl: 'uploader/download/' + data.image_history_id
                                });

                                toastr.success("Zip file has been created successfully.");
                            } else {

                                totalImageProcess++;

                                let percent = Math.round((processCompleted / totalImages) * 100);

                                Event.emit('progress', {percent})
                            }
                        }
                    } else {
                        Event.emit('finished_loading');
                    }
                });
            }
        });
    }


    /**
     * Show toastr notification to the user
     * when zip file download link copied
     */
    copied() {
        toastr.success("Zip file download link copied to clipboard.");
    }


    /**
     * Get image upload directory name.
     */
    getImageUploadDirectory() {
        let req = Request.post('/create-image-upload-directory');

        req.set( { directory: 'default' } )
        req.end((err, res) => {
            if (err) if (err) toastr.error(err, 'Error');

            if(res.ok) {
                let response = JSON.parse(res.text)

                window.directory = response.directoryName;
            }
        });
    }
}


export default ImageUploader;