<?php

namespace ThemeXpert\Commands;

use ThemeXpert\Models\User;

class UserCommand
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}