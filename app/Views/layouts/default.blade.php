<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Image Placeholder and WordPress Dummy Image Replacer - Faker.IM</title>
    <meta name="og:title" content="WordPress Dummy image replacer and placeholder service - Faker"/>
     <meta name="og:image" content="http://faker.im/600x400?text=Hello+Faker"/>
     <meta name="og:url" content="http://faker.im/"/>
     <meta name="og:description" content="Faker will replace your theme premium image with fake placeholder on the fly. You can use it as a placeholder image provider."/>

    <!-- Favicon -->
    <link href="{{ url('/assets/images/favicon.ico') }}" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
    <link rel="stylesheet" href="{{ url('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/jquery.mmenu.all.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{ url('/assets/css/main.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"/>
</head>
<body>
  <header class="tx-header">
      <div class="container">
          <a href="{{ url('/') }}" class="logo"><img src="{{ url('/assets/images/faker_logo.png') }}" alt="Faker Logo"></a>
          <nav class="main-menu">
              <a href="{{ url('/uploader') }}" class="menu-about">Upload Images</a>
              <a href="javascript:void(0)" class="menu-about" data-menu="about">About</a>
              <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8JGBY7RRMNWAS" target="_blank" class="donate"><span class="glyphicon glyphicon-heart"></span> Donate</a>
          </nav>
      </div>
  </header><!--/.tx-header-->

  <section id="main">

      @yield('content')

       <div class="container about">
          <div class="container">
              <div class="close-btn clearfix"><a href="javascript:void(0)" class="closebtn">×</a></div>
              <div class="overlay-content">
                  <h2>Why we made this</h2>
                  <p>Replacing premium images from themes is an incredibly important task for any theme developer. More image you use more time it takes to replace them.</p>

                  <p>We are a close-knit team of theme developers and time is extremely valuable for us. We can't afford 30-60 minutes of our time for just images replacement.</p>

                  <p>We looked for a better solution. Unfortunately, there are nothing that can solve our problems.</p>

                  <p>That’s why we started Faker.im</p>

                  <p>A simple tool to replace your server image path with a fake image without changing the image size. When your client imports your dummy xml he will get exact same sized but fake image served by Faker.IM</p>

                  <p>
                  Regards, <br/>
                  Parvez Akther <br/>
                  CEO, <a href="https://www.themexpert.com/?utm_source=faker.im&utm_medium=about&utm_campaign=faker.im" target="_blank">ThemeXpert</a>
                  </p>
              </div>
          </div>
      </div>

  </section>

  <footer>
      <div class="container text-center">
          Made with <span class="glyphicon glyphicon-heart"></span> at <a href="https://www.themexpert.com/?utm_source=faker.im&utm_medium=footer&utm_campaign=faker.im" target="_blank">ThemeXpert</a>
      </div>
  </footer>

  <script type="text/javascript">window.url = "{{ url('/')  }}"</script>
  <script type="text/javascript" src="{{ url('/assets/js/jquery-1.12.3.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('/assets/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('/assets/js/jquery.mmenu.all.min.js') }}"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script type="text/javascript" src="{{ url('/assets/js/app.js') }}"></script>
  <script type="text/javascript" src="{{ url('/assets/js/components.js') }}"></script>
  <script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/541957995000fc7f080000a0.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
  </script>
</body>
</html>
