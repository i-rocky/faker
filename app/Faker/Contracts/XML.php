<?php

namespace ThemeXpert\Faker\Contracts;

interface XML
{
    /**
     * Get fake XML file for the faker.im.
     *
     * @param $xml
     *
     * @return mixed|void
     */
    public function getFakeXML($xml);

    /**
     * Download fake XML file.
     *
     * @param $originalXML
     * @param $fileName
     *
     * @return mixed
     */
    public function downloadFakeXML($originalXML, $fileName);
}