<?php

namespace ThemeXpert\Consoles;

use DB;
use Carbon\Carbon;
use Sun\Console\Command;
use Sun\Contracts\Application;
use Sun\Contracts\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CleanExpiredZipFiles extends Command
{
    /**
     * @var string Command name
     */
    protected $name = 'clean:expired-zip';

    /**
     * @var string Command description
     */
    protected $description = "Clean all expired zip files";

    /**
     * Instance of filesystem.
     *
     * @var \Sun\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * @param Application $app
     * @param Filesystem  $filesystem
     */
    public function __construct(Application $app, Filesystem $filesystem)
    {
        parent::__construct();
        $this->app = $app;

        $this->filesystem = $filesystem;
    }

    /**
     * To handle console command
     */
    public function handle()
    {
        $zipFiles = DB::table('image_histories')
                        ->where('created_at', '<=', Carbon::now()->subMinutes(30)->toDateTimeString())
                        ->get();

        $rootPath = storage_path() . '/app/uploads/';

        if( count($zipFiles) > 0 ) {
            foreach($zipFiles as $zipFile) {
                $this->filesystem->cleanDirectory($rootPath . $zipFile->directory, true);

                @unlink($rootPath . $zipFile->directory . ".zip");
            }

            DB::table('image_histories')
                ->where('created_at', '<=', Carbon::now()->subMinutes(30)->toDateTimeString())
                ->delete();
        }

        $this->success('All expired zip files has been removed successfully.');
    }

    /**
     * Set your command arguments
     *
     * @return array
     */
    protected function getArguments()
    {
        return [ ];
    }

    /**
     * Set your command options
     *
     * @return array
     */
    protected function getOptions()
    {
        return [ ];
    }
}