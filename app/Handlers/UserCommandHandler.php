<?php

namespace ThemeXpert\Handlers;

use ThemeXpert\Commands\UserCommand;

class UserCommandHandler
{
    /**
     * To handle command
     *
     * @param UserCommand $command
     *
     * @return mixed
     */
    public function handle(UserCommand $command)
    {
        return $command->user->name;
    }
}