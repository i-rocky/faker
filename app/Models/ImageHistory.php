<?php

namespace ThemeXpert\Models;

use Sun\Database\Model;

class ImageHistory extends Model
{
    protected $table = 'image_histories';

    protected $fillable = ['directory'];

}