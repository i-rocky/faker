<?php

use Carbon\Carbon;
use Sun\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImageHistory extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        Schema::create('image_histories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('directory')->index();
            $table->timestamp('created_at')->default(Carbon::now()->toDateTimeString());
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Schema::drop('image_histories');
    }
}